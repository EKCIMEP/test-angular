This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.0.

## Install NPM package

Run `npm i`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
