import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {AppModule} from '../app.module';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'app-rep',
    templateUrl: './rep.component.html',
    styleUrls: ['./rep.component.css']
})
export class RepComponent implements OnInit {
    public username: string;
    public rep: string;

    public author: string = localStorage.getItem('name');

    public commitsList: object;
    public branchesList: object;

    public condition: boolean = true;

    constructor(private _http: Http, private _route: ActivatedRoute, private _location: Location) {
        this.username = _route.snapshot.params['username'];
        this.rep = _route.snapshot.params['rep'];
    }

    ngOnInit() {
        const url = AppModule.url;
        this._http.get(url + '/repos/' + this.username + '/' + this.rep)
            .subscribe((res: Response) => {
                const obj = res.json();

                const branchesUrl = obj.branches_url.replace('{/branch}', '');
                const commitUrl = obj.commits_url.replace('{/sha}', '');

                this._http.get(branchesUrl).subscribe((branches: Response) => {
                    this.branchesList = branches.json();
                });

                this._http.get(commitUrl).subscribe((commits: Response) => {
                    this.commitsList = commits.json();
                });

            });
    }

    public openTab() {
        this.condition = !this.condition;
    }

    public backClick() {
        this._location.back();
    }
}
