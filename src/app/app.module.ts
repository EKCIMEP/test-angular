import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {HttpModule} from '@angular/http';
import {UserComponent} from './user/user.component';
import {RepComponent} from './rep/rep.component';
import {MainComponent} from './main/main.component';
import {HeaderComponent} from './header/header.component';

const appRoutes: Routes = [
    {path: '', component: MainComponent, pathMatch: 'full'},
    {path: 'list', component: AppComponent, pathMatch: 'full'},
    {path: 'user/:username', component: UserComponent, pathMatch: 'full'},
    {path: 'user/:username/:rep', component: RepComponent, pathMatch: 'full'},
    {path: '**', redirectTo: '/'}
];

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        UserComponent,
        RepComponent,
        MainComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
    public static url = 'https://api.github.com';
}
