import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {AppModule} from '../app.module';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
    public users: any[];
    public next: any;
    public current: any;
    public prev: any;
    private i: any = 1;

    constructor(private _http: Http) {
        localStorage.clear();
    }

    ngOnInit() {
        const url = AppModule.url;
        this.prev = url + '/users';
        this.current = url + '/users';
        this._http.get(url + '/users').subscribe(
            (res: Response) => {
                const link = res.headers.get('link')
                    .replace(/rel="next"|[<;>]/g, '')
                    .split(',');
                this.next = link[0];
                this.users = res.json();
            }
        );
    }

    public pagPrev() {
        this._http.get(this.prev).subscribe(
            (res: Response) => {
                const link = res.headers.get('link')
                    .replace(/rel="next"|[<;>]/g, '')
                    .split(',');
                this.next = link[0];
                this.users = res.json();
                this.i -= 1;
            }
        );
    }

    public pagNext() {
        this.prev = this.current;
        this._http.get(this.next).subscribe(
            (res: Response) => {
                const link = res.headers.get('link')
                    .replace(/rel="next"|[<;>]/g, '')
                    .split(',');
                this.current = this.next;
                this.next = link[0];
                this.users = res.json();
                this.i += 1;
            }
        );
    }

}
