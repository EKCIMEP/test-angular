import {Component, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {AppModule} from '../app.module';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    public user: object;
    public username: string;

    constructor(private _http: Http, private _route: ActivatedRoute, private _location: Location) {
        this.username = _route.snapshot.params['username'];
    }

    ngOnInit() {
        const url = AppModule.url;
        this._http.get(url + '/users/' + this.username)
            .subscribe((res: Response) => {
                const obj = res.json();
                this.user = {
                    login: obj.login,
                    name: obj.name,
                    email: obj.email || '-',
                    photo: obj.avatar_url,
                };
                localStorage.setItem('name', obj.name);
            });

        this._http.get(url + '/users/' + this.username + '/repos')
            .subscribe((res: Response) => {
                this.user['repositories'] = res.json();
            });
    }

    public backClick() {
        this._location.back();
    }
}
